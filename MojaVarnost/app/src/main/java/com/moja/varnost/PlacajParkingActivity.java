package com.moja.varnost;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class PlacajParkingActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private EditText registrskaText;
    private TextView cona1Text;
    private TextView cona2Text;
    private TextView cona3Text;
    private EditText uraText;
    private TextView podropnostiOParkiranju;

    private double LAT;
    private double LON;

    private String izbranaCona = "1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_placaj_parking);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) mapFragment.getMapAsync(this);

            // LAT in LON trenutne lokacije:
        LAT = getIntent().getDoubleExtra("LAT", 0);
        LON = getIntent().getDoubleExtra("LON", 0);

        registrskaText = findViewById(R.id.reg);
        cona1Text = findViewById(R.id.con1);
        cona2Text = findViewById(R.id.con2);
        cona3Text = findViewById(R.id.con3);
        podropnostiOParkiranju = findViewById(R.id.text_view_podrobnost_parkiranja);
        uraText = findViewById(R.id.edit_text_ura);

    }

        // Nazaj na glavni Activity.
    public void nazaj(View view) {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

        // Pošlje SMS z intentom.
    public void posljiSms(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.button_blink));
        String registrska = registrskaText.getText().toString();
        registrska = registrska.replace(" ", "");
        registrska = registrska.replace("-", "");
        Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( "sms:" + "040202010"));
        intent.putExtra( "sms_body", "C" + izbranaCona + " " + registrska + " " + uraText.getText().toString());
        startActivity(intent);
    }

        // Kaj se zgodi, ko klikne na cono1/2/3:
    public void conaIzbrana(View view) {
        TextView tv;
        switch (view.getId()) {
            case R.id.con1:
                tv = findViewById(R.id.con1);
                tv.setTypeface(null, Typeface.BOLD);
                view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.bounce));
                // Izbrana cona 1.
                izbranaCona = "1";

                tv = findViewById(R.id.con2);
                tv.setTypeface(null, Typeface.NORMAL);
                tv = findViewById(R.id.con3);
                tv.setTypeface(null, Typeface.NORMAL);

                podropnostiOParkiranju.setText(R.string.string_cona1);
                break;

            case R.id.con2:
                tv = findViewById(R.id.con1);
                tv.setTypeface(null, Typeface.NORMAL);
                tv = findViewById(R.id.con2);
                tv.setTypeface(null, Typeface.BOLD);
                view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.bounce));
                // Izbrana cona 2.
                izbranaCona = "2";

                tv = findViewById(R.id.con3);
                tv.setTypeface(null, Typeface.NORMAL);

                podropnostiOParkiranju.setText(R.string.string_cona2);
                break;

            case R.id.con3:
                tv = findViewById(R.id.con1);
                tv.setTypeface(null, Typeface.NORMAL);
                tv = findViewById(R.id.con2);
                tv.setTypeface(null, Typeface.NORMAL);
                tv = findViewById(R.id.con3);
                tv.setTypeface(null, Typeface.BOLD);
                view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.bounce));
                // Izbrana cona 3.
                izbranaCona = "3";

                podropnostiOParkiranju.setText(R.string.string_cona3);
                break;
        }
    }

        // onMapReady(..): ################################################################################################################################################################################################

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style));
            if (!success) {
                Log.e("MAPS_ACTIVITY", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("MAPS_ACTIVITY", "Can't find style. Error: ", e);
        }

        // Add a marker in Sydney and move the camera
        LatLng MyLocation = new LatLng(LAT, LON);
        MarkerOptions marker = new MarkerOptions().position(MyLocation).title("Moja lokacija");
        // Changing marker icon
        marker.icon(vectorToBitmap());
        mMap.addMarker(marker);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MyLocation, 14));
    }

    private BitmapDescriptor vectorToBitmap() {
        Drawable vectorDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_map_my_location_on_map_icon, null);
        assert vectorDrawable != null;
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        // DrawableCompat.setTint(vectorDrawable, color);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public void odpriMain(View view) {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }
    public void odpriNastavitve(View view) {
        Intent intent = new Intent(this, Nastavitve.class);
        startActivity(intent);
    }
    public void najblizjaPolicijska(View view) {
        Double lat1 = MapsActivity.getLat();
        Double lon1 = MapsActivity.getLon();
        ArrayList<Marker> markerjiPolicije = MapsActivity.getPolicaje();

        Double minDistance = 100000.0;
        int minDistIndex = 0;
        final int R = 6371;
        for (int i = 0;  i < markerjiPolicije.size(); i++) {
            Double lat2 = markerjiPolicije.get(i).getPosition().latitude;
            Double lon2 = markerjiPolicije.get(i).getPosition().longitude;
            Double latDistance = toRad(lat2 - lat1);
            Double lonDistance = toRad(lon2 - lon1);
            Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
            Double distance = R * 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            if (distance < minDistance) {
                minDistance = distance;
                minDistIndex = i;
            }
        }
        String options = "google.navigation:q=" + markerjiPolicije.get(minDistIndex).getPosition().latitude + "," + markerjiPolicije.get(minDistIndex).getPosition().longitude;
        Intent navigation = new Intent(Intent.ACTION_VIEW, Uri.parse(options));
        startActivity(navigation);
    }
    private static Double toRad(Double value) {
        return value * Math.PI / 180;
    }
}