package com.moja.varnost;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;

public class SOSActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private double LAT;
    private double LON;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sos);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) mapFragment.getMapAsync(this);

        // LAT in LON trenutne lokacije:
        LAT = getIntent().getDoubleExtra("LAT", 0);
        LON = getIntent().getDoubleExtra("LON", 0);
    }

    public void kliciResilce(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.button_blink));
        Uri number = Uri.parse("tel:112");
        Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
        startActivity(callIntent);
    }

    public void kliciPolicijo(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.button_blink));
        Uri number = Uri.parse("tel:113");
        Intent callIntent = new Intent(Intent.ACTION_DIAL, number);
        startActivity(callIntent);
    }

    public void nazaj2(View view) {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style));
            if (!success) {
                Log.e("MAPS_ACTIVITY", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("MAPS_ACTIVITY", "Can't find style. Error: ", e);
        }

        // Add a marker in Sydney and move the camera
        LatLng MyLocation = new LatLng(LAT, LON);
        MarkerOptions marker = new MarkerOptions().position(MyLocation).title("Moja lokacija");
        // Changing marker icon
        marker.icon(vectorToBitmap());
        mMap.addMarker(marker);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MyLocation, 14));
    }

    private BitmapDescriptor vectorToBitmap() {
        Drawable vectorDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_map_my_location_on_map_icon, null);
        assert vectorDrawable != null;
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        // DrawableCompat.setTint(vectorDrawable, color);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public void odpriMain(View view) {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }
}