package com.moja.varnost;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;

public class PrvaPomoc extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prva_pomoc);
    }
    public void odpriMain(View view) {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }
    public void odpriNastavitve(View view) {
        Intent intent = new Intent(this, Nastavitve.class);
        startActivity(intent);
    }
    public void najblizjaPolicijska(View view) {
        Double lat1 = MapsActivity.getLat();
        Double lon1 = MapsActivity.getLon();
        ArrayList<Marker> markerjiPolicije = MapsActivity.getPolicaje();

        Double minDistance = 100000.0;
        int minDistIndex = 0;
        final int R = 6371;
        for (int i = 0;  i < markerjiPolicije.size(); i++) {
            Double lat2 = markerjiPolicije.get(i).getPosition().latitude;
            Double lon2 = markerjiPolicije.get(i).getPosition().longitude;
            Double latDistance = toRad(lat2 - lat1);
            Double lonDistance = toRad(lon2 - lon1);
            Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
            Double distance = R * 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            if (distance < minDistance) {
                minDistance = distance;
                minDistIndex = i;
            }
        }
        String options = "google.navigation:q=" + markerjiPolicije.get(minDistIndex).getPosition().latitude + "," + markerjiPolicije.get(minDistIndex).getPosition().longitude;
        Intent navigation = new Intent(Intent.ACTION_VIEW, Uri.parse(options));
        startActivity(navigation);
    }
    private static Double toRad(Double value) {
        return value * Math.PI / 180;
    }
}
