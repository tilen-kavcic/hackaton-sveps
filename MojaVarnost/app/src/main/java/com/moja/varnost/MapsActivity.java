package com.moja.varnost;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;

import androidx.annotation.DrawableRes;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.FragmentActivity;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import im.delight.android.location.SimpleLocation;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private GoogleMap mMap;

    private int policijaSt = 0;
    private int bolnicaSt = 0;

    public static double LAT = 0;
    public static double LON = 0;
    public static ArrayList<Marker> markerjiBolnic = new ArrayList<Marker>();
    public static ArrayList<Marker> markerjiPolicije = new ArrayList<Marker>();
    private static final int requestPermissionID = 200;

    private SimpleLocation location;

    public static ArrayList<Marker> getPolicaje() {
        return markerjiPolicije;
    }
    public static double getLat() {
        return LAT;
    }
    public static double getLon() {
        return LON;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) mapFragment.getMapAsync(MapsActivity.this);

        location = new SimpleLocation(this);
        // if we can't access the location yet
        if (!location.hasLocationEnabled()) {
            // ask the user to enable location access
            SimpleLocation.openSettings(this);
        }

/*
        ImageButton infoinfo = (ImageButton) findViewById(R.id.info);
        infoinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.startAnimation(AnimationUtils.loadAnimation(infoinfo, R.anim.button_blink));
                startActivity(new Intent(MapsActivity.this, infobot.class));
            }
        });
*/

        // Get user location:
        LAT = location.getLatitude();
        LON = location.getLongitude();

        // Log the location under 'USER LOCATION':
        Log.d("USER LOCATION", location.toString());

        if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(MapsActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, requestPermissionID);
            return;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        location.beginUpdates();
    }
    @Override
    protected void onPause() {
        location.endUpdates();
        super.onPause();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        try {
            // Customise the styling of the base map using a JSON object defined in a raw resource file.
            boolean success = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.style));
            if (!success) {
                Log.e("MAPS_ACTIVITY", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("MAPS_ACTIVITY", "Can't find style. Error: ", e);
        }

        prikaziBolnicePrvic();
        brisiBolnice();
        prikaziPolicijoPrvic();
        brisiPolicijo();

        LatLng MyLocation = new LatLng(LAT, LON);
        MarkerOptions marker = new MarkerOptions().position(MyLocation).title("Moja lokacija");
        // Changing marker icon
        marker.icon(vectorToBitmap(R.drawable.ic_map_my_location_on_map_no_arrow));
        mMap.addMarker(marker);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MyLocation, 14));
    }

    private void najblizjaPolicijska() {
        Double lat1 = LAT;
        Double lon1 = LON;
        Double minDistance = 100000.0;
        int minDistIndex = 0;
        final int R = 6371;
        for (int i = 0;  i < markerjiPolicije.size(); i++) {
            Double lat2 = markerjiPolicije.get(i).getPosition().latitude;
            Double lon2 = markerjiPolicije.get(i).getPosition().longitude;
            Double latDistance = toRad(lat2 - lat1);
            Double lonDistance = toRad(lon2 - lon1);
            Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
            Double distance = R * 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            if (distance < minDistance) {
                minDistance = distance;
                minDistIndex = i;
            }
        }
        String options = "google.navigation:q=" + markerjiPolicije.get(minDistIndex).getPosition().latitude + "," + markerjiPolicije.get(minDistIndex).getPosition().longitude;
        Intent navigation = new Intent(Intent.ACTION_VIEW, Uri.parse(options));
        startActivity(navigation);
    }
    private static Double toRad(Double value) {
        return value * Math.PI / 180;
    }

    public void centrirajMe(View view) {
        LAT = location.getLatitude();
        LON = location.getLongitude();
        LatLng MyLocation = new LatLng(LAT, LON);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MyLocation, 14));

    }

    private void prikaziBolnicePrvic() {
        Double[][] bolnice = new Double[][]{{14.5247825,46.0536077},{14.2019099,46.3766812},{14.2105347,45.7722339},{14.487361,46.0690035},{14.4867966,46.0680191},{14.5202926,46.0548693},{14.5234539,46.0532476},{15.260609,46.2324647},{14.5693615,46.0518964},{14.8054871,45.9376381},{13.6851455,45.5449708},{16.1856969,46.6484582},{13.5781474,46.2472883},{14.5765075,45.9807367},{15.576683,46.4831385},{14.4866222,46.0465963},{14.5243584,46.0680333},{13.6604463,45.5345953},{13.8672205,45.7094772},{15.6492422,46.5510318},{14.5109652,46.0647107},{15.875755,46.4279247},{15.876253,46.4263035},{15.8760827,46.4260491},{15.8752805,46.4260436},{14.5232921,46.0538629},{14.5205663,46.0513673},{14.5255375,46.0520319},{14.5222314,46.0532489},{14.5216935,46.0555743},{14.5169451,46.0515801},{14.6130757,46.2216688},{15.6267712,46.4562998},{14.524643,46.0541296},{14.5150286,46.056852},{14.5290702,46.0552976},{15.2629202,46.2320416},{13.7322074,45.5396937},{14.4811128,46.0463217},{14.5318629,46.1031662},{14.5315846,46.0562106},{14.5007116,46.0608944},{14.570991,46.1622816},{15.0812473,46.5092984},{16.2339155,46.609569},{14.5703774,46.0530784},{14.5708819,46.0532829},{14.5709798,46.0535653},{14.5708422,46.0542188},{14.5714588,46.0533079},{14.5714715,46.0523933},{14.5705273,46.0534542},{14.5895518,46.0527897},{15.8284543,46.5773109},{15.0817594,46.506904},{13.6401127,45.9319536},{14.7471923,46.1373708},{15.6715022,46.5235822},{15.4832193,45.943116},{15.5780869,46.3130359},{14.312689,46.1662526},{15.1622176,45.8014829},{15.1636645,45.8018601},{15.1639808,45.8012508},{15.1630882,45.8001691},{15.1619242,45.8008117},{15.1617983,45.8004398},{14.0378893,46.4434432},{13.9447136,45.6061148},{16.4566397,46.5584298},{16.2957147,46.5687986},{14.2933391,45.9629571},{16.3123751,46.626035},{14.3656569,45.7915497},{14.4828394,46.0713751},{14.5963693,46.1398576},{14.5023533,46.0645199},{15.640403,46.2389391},{15.6408477,46.2388383},{15.6340617,46.2393506},{14.6776918,46.0940015},{14.4055907,46.1457838},{14.2101056,45.7719086},{14.3531117,46.2487383},{14.1087336,46.3715951},{14.353749,46.2485413},{14.354152,46.2486785},{14.0449418,46.4402939},{16.141516,46.4065297},{16.141516,46.4065297},{15.5937925,45.9089144},{15.5939863,45.9092976},{13.7305527,45.5804744},{15.9026067,46.3692239},{15.0202028,46.4049219},{14.6528267,45.9597725}};
        for (int i = 0; i < bolnice.length; i++) {
            LatLng latLng = new LatLng(bolnice[i][1], bolnice[i][0]);
            Marker markerBolnic = mMap.addMarker(new MarkerOptions().position(latLng).title("Bolnišnica").icon(vectorToBitmap(R.drawable.ic_bolnisnica)));
            markerjiBolnic.add(markerBolnic);
        }
    }
    private void brisiBolnice() {
        for(int i = 0; i < markerjiBolnic.size(); i++) {
            markerjiBolnic.get(i).setVisible(false);
        }
    }
    private void prikaziBolnice() {
        for(int i = 0; i < markerjiBolnic.size(); i++) {
            markerjiBolnic.get(i).setVisible(true);
        }
    }

    private void prikaziPolicijoPrvic() {
        Double[][] policijske = new Double[][]{{45.8981942,15.5937641},{45.5962968,15.1716356},{45.757132999999996,15.0578034},{45.9625402,15.486007700000002},{45.843182899999995,15.419410499999998},{45.8300487,15.507124000000001},{45.647048,15.314160399999999},{45.817003,15.1549594},{46.0078949,15.3043035},{45.8440902,15.338860499999997},{45.9084381,15.007671199999999},{45.8983451,15.654876799999998},{45.8871798,13.9046814},{46.0012785,14.029840799999999},{45.961135399999996,13.6519622},{46.1818514,13.7348749},{46.332728499999995,13.541973299999999},{45.9243123,13.629696899999999},{45.8991427,13.6108554},{45.942024499999995,13.636831899999999},{46.6592512,16.1686722},{46.805166899999996,16.035096},{46.827635799999996,16.3250435},{46.662782899999996,16.0109152},{46.8051434,16.2173665},{46.5174844,16.1958363},{46.651721699999996,16.3520742},{46.5184918,16.4409097},{46.4163127,15.9998544},{46.571458299999996,16.4479448},{46.584671799999995,15.841731600000001},{46.500788799999995,15.7526161},{46.558706,15.653048199999999},{46.543897300000005,15.628157199999999},{46.4097418,16.149238999999998},{46.336207699999996,15.878924},{46.4273749,15.882564400000001},{46.453677,15.675371499999997},{46.5403953,15.511967799999999},{46.3921302,15.576194500000001},{46.6778998,15.654600900000002},{46.287303,15.858244299999999},{46.349753799999995,15.735799600000002},{46.39490250000001,16.275301},{46.389641000000005,16.051375999999998},{46.541860899999996,15.652737600000002},{46.5318183,15.6689752},{46.6897196,15.645448400000001},{46.2317663,15.260367799999997},{46.5867182,15.023605499999999},{46.1551811,15.232648399999999},{46.338826499999996,14.960194},{46.613856299999995,15.227158999999999},{46.543701600000006,14.9473182},{46.224211399999994,15.642547899999999},{46.5119307,15.0795893},{46.2168822,15.394715899999998},{46.3419162,15.432279699999999},{46.2290414,15.5212968},{46.3556807,15.1157925},{46.252671299999996,15.159919799999997},{46.3565572,15.120861999999999},{46.069932300000005,15.6605912},{46.2558072,15.252641100000002},{46.224211399999994,15.642547899999999},{45.57265290000001,14.2364818},{45.5388449,13.731736199999999},{45.5350964,13.660036},{45.609552699999995,13.9382551},{45.5080093,13.614669899999999},{45.7774188,14.2180103},{45.7062272,13.8711742},{45.4556553,13.6578299},{45.4645645,13.8932605},{45.4921772,14.2127287},{45.5486839,13.7253834},{45.500420399999996,14.2733517},{45.566889499999995,13.781889699999999},{45.566889499999995,13.781889699999999},{45.621949099999995,13.9047897},{46.369101,14.1160591},{46.4482286,14.0137101},{46.2451705,14.3544451},{46.4867397,13.7784777},{46.3432853,14.174818799999999},{46.1714157,14.3301629},{46.3536017,14.2935169},{46.2307558,14.454779600000002},{46.2451705,14.3544451},{46.2307558,14.454779600000002},{46.4482286,14.0137101},{45.7924072,14.3596518},{46.142108799999995,14.594711499999999},{45.955544499999995,14.653536599999997},{46.141103799999996,15.085312},{46.226743299999995,14.61229},{46.057280999999996,14.823541299999997},{46.071244799999995,14.509918100000002},{45.6458667,14.852370899999999},{46.0610225,14.533640799999999},{46.055394299999996,14.5069058},{46.0373745,14.476625700000001},{45.9184139,14.2278548},{46.1386032,14.4138405},{45.740749,14.7250262},{46.0725914,14.467511799999999},{45.9639278,14.295231000000001},{46.155906699999996,15.052549899999999},{46.135024099999995,14.993289599999999},{46.0882046,14.522557499999998},{46.0598269,14.450531900000001},{46.0208028,14.442486100000002},{46.0560536,14.584364}};
        for (int i = 0; i < policijske.length; i++) {
            LatLng latLng = new LatLng(policijske[i][0], policijske[i][1]);
            Marker markerPolicije = mMap.addMarker(new MarkerOptions().position(latLng).title("Policijska postaja").icon(vectorToBitmap(R.drawable.ic_policija)));
            markerjiPolicije.add(markerPolicije);
        }
    }
    private void brisiPolicijo() {
        for(int i = 0; i < markerjiPolicije.size(); i++) {
            markerjiPolicije.get(i).setVisible(false);
        }
    }
    private void prikaziPolicijo() {
        for(int i = 0; i < markerjiPolicije.size(); i++) {
            markerjiPolicije.get(i).setVisible(true);
        }
    }

    // prikaze kao najblizjo avtomatsko masino pa bolnice, skrije kapse
    private void prikaziMasineZaOzivlat() {
        LatLng latLng = new LatLng(46.060929, 14.533858);
        Marker markerAED = mMap.addMarker(new MarkerOptions().position(latLng).title("AED").icon(vectorToBitmap(R.drawable.ic_map_defiblirator_icon)));
        brisiPolicijo();
        prikaziBolnice();
    }

    private BitmapDescriptor vectorToBitmap(@DrawableRes int id) {
        Drawable vectorDrawable = ResourcesCompat.getDrawable(getResources(), id, null);
        assert vectorDrawable != null;
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        // DrawableCompat.setTint(vectorDrawable, color);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    public void SOS(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.button_blink));
        Intent intent = new Intent(this, SOSActivity.class);
        intent.putExtra("LAT", LAT);
        intent.putExtra("LON", LON);
        startActivity(intent);
    }

    public void InfoBot (View view){
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.button_blink));
        startActivity(new Intent(MapsActivity.this, Animation.class));
    }

    public void placajParking(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.button_blink));
        Intent intent = new Intent(this, PlacajParkingActivity.class);
        intent.putExtra("LAT", LAT);
        intent.putExtra("LON", LON);
        startActivity(intent);
    }

    public void prijaviPrekrsek(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.button_blink));
        Intent intent = new Intent(this, PrijaviPrekrsek.class);
        startActivity(intent);
    }

    public void prvaPomoc(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.button_blink));
        Intent intent = new Intent(this, PrvaPomoc.class);
        startActivity(intent);
    }

    public void pogresaneOsebe(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.button_blink));
        Intent intent = new Intent(this, PogresaneOsebe.class);
        startActivity(intent);
    }

    public void najblizjaPolicijskaGumb(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.button_blink));
        najblizjaPolicijska();
    }

    public void odpriNastavitve(View view) {
        Intent intent = new Intent(this, Nastavitve.class);
        startActivity(intent);
    }

    public void toogleBolnisnice(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate));
        if (bolnicaSt == 0) {prikaziBolnice(); bolnicaSt = 1;}
        else {brisiBolnice(); bolnicaSt = 0;}
    }

    public void tooglePolicijskePostaje(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate));
        if (policijaSt == 0) {prikaziPolicijo(); policijaSt = 1;}
        else {brisiPolicijo(); policijaSt = 0;}
    }
}
